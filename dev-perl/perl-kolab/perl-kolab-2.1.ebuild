# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils perl-module

MY_P=perl-kolab-2.1-gentoo-20070510

DESCRIPTION="Perl modules for the Kolab groupware server"
HOMEPAGE="http://www.kolab.org"
SRC_URI="http://files.pardus.de/${MY_P}.tar.bz2"

SLOT="0"

LICENSE="GPL-2"
KEYWORDS="x86 amd64"
IUSE=""

DEPEND=""

RDEPEND="dev-lang/perl
		 perl-core/DB_File

		 >net-mail/cyrus-imap-admin-2.3.12

		 dev-perl/perl-ldap
		 dev-perl/MIME-tools
		 dev-perl/MIME-Lite
		 dev-perl/Mail-IMAPClient
		 dev-perl/URI"

S=${WORKDIR}/${MY_P}

pkg_setup() {
	#############################################################
	# Create the special kolab users and their groups

	# The kolab groups will have the same name as the kolab users
	local musr="kolab"
	local rusr="kolab-r"
	local nusr="kolab-n"

	local kolabusers="${musr}:kolab-management-account
					  ${rusr}:kolab-restricted-account
					  ${nusr}:kolab-non-priviledged-account"

	for user in ${kolabusers}
	do
	  enewgroup ${user/:*/}
	  enewuser  ${user/:*/} -1 /bin/bash / ${user/:*/} "-cadded by portage for kolab [${user/*:/}]"
	done
}

src_unpack() {

	unpack ${A} && cd "${S}"

	## Patching the code for gentoo

	for PATCH in ${FILESDIR}/*-${PV}.patch
	do
		epatch ${PATCH}
	done
}

src_compile() {

	perlinfo

	local myconf
	myconf="${myconf} --localstatedir=/var"
	myconf="${myconf} --with-dist=gentoo"
	myconf="${myconf} --libexecdir=/usr/lib"

	econf ${myconf} || die
	emake || die
}

src_install() {

	#############################################################
	# Install perl source code

	make install DESTDIR=${D} || die

	# Add this script until we have a better solution
	dosbin ${FILESDIR}/kolab_services
}
