# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module

MY_PV=${PV/_alpha/_gentoo}

DESCRIPTION="Perl modules and scripts for the Kolab groupware server"
HOMEPAGE="http://www.kolab.org"
SRC_URI="http://files.pardus.de/${PN}-${MY_PV}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64"
IUSE=""

SLOT="0"

DEPEND="dev-lang/perl
	perl-core/DB_File
	>=net-mail/cyrus-imap-admin-2.3.12
	dev-perl/perl-ldap
	dev-perl/MIME-tools
	dev-perl/MIME-Lite
	dev-perl/Mail-IMAPClient
	dev-perl/URI"

S=${WORKDIR}/${PN}-${MY_PV}

src_compile() {

	myconf="--config=/etc/kolab/kolab.globals"
	myconf="${myconf} --bin=\"${ROOT}/usr/bin\""

	perl-module_src_compile
}
