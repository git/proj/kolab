KOLAB_META_START
TARGET=/etc/apache2/vhosts.d/13_kolab_example.conf
PERMISSIONS=0640
OWNERSHIP=root:root
KOLAB_META_END
# this file is automatically written by the Kolab config backend
# manual additions are lost unless made to the template in the Kolab config directory

# #----------------------------------------------------------------------
# # WEBROOT FOR THE DOMAIN 
# #
# # Install all non-kolab stuff here
# #----------------------------------------------------------------------

# <Directory "/var/www/@MYDOMAIN@/cgi-bin">
#   AllowOverride All
#   Options ExecCGI
#   <IfModule mod_authz_host.c>
#     Order allow,deny
#     Allow from all
#   </IfModule>
# </Directory>

# <Directory "/var/www/@MYDOMAIN@/htdocs">
#   Options Indexes FollowSymLinks
#   AllowOverride None
#   <IfModule mod_authz_host.c>
#     Order allow,deny
#     Allow from all
#   </IfModule>
# </Directory>

# <Directory "/var/www/@MYDOMAIN@/htdocs/private">
#    <IfModule mod_authz_host.c>
#      Allow from all
#      <IfModule authnz_ldap_module>
#        AuthType Basic
#        AuthName "Kolab Domain DAV"
#        AuthBasicProvider ldap-mail ldap-uid
#        Require ldap-group "cn=@MYDOMAIN@,cn=domains,cn=internal"
#      </IfModule>
#    </IfModule>
# </Directory>

# Include /etc/apache2/vhosts.d/@MYDOMAIN@-base-*.include

# #----------------------------------------------------------------------
# # DOMAIN VHOST AND SSL VHOST
# #
# # 
# #----------------------------------------------------------------------

# <VirtualHost *:80>
#   DocumentRoot "/var/www/@MYDOMAIN@/htdocs"
#   ServerName @MYDOMAIN@
#   ServerAlias www.@MYDOMAIN@
#   ServerAdmin hostmaster@@MYDOMAIN@
#   ErrorLog /var/log/apache2/@MYDOMAIN@_error_log
#   CustomLog /var/log/apache2/@MYDOMAIN@_access_log combined
#   Include /etc/apache2/vhosts.d/kolab-domain-*.include
#   Include /etc/apache2/vhosts.d/@MYDOMAIN@-domain-*.include
# </VirtualHost>

# <IfDefine SSL>
#   <IfDefine MULTIPLE_DOMAINS>
#     <IfModule mod_ssl.c>
#       <VirtualHost *:443>
# 	DocumentRoot "/var/www/@MYDOMAIN@/htdocs"
# 	ServerName @MYDOMAIN@:443
# 	ServerAdmin hostmaster@@MYDOMAIN@
# 	ErrorLog /var/log/apache2/@MYDOMAIN@_ssl_error_log
# 	CustomLog /var/log/apache2/@MYDOMAIN@_ssl_access_log combined

# 	SSLEngine on
# 	SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP:+eNULL
# 	SSLCertificateFile    /etc/kolab/cert.pem
# 	SSLCertificateKeyFile /etc/kolab/key.pem

# 	<Files ~ "\.(cgi|shtml|phtml|php?)$">
# 	  SSLOptions +StdEnvVars
# 	</Files>
# 	<Directory "/var/www/@MYDOMAIN@/cgi-bin">
# 	  SSLOptions +StdEnvVars
# 	</Directory>
# 	<IfModule mod_setenvif.c>
# 	  SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown \
# 	  downgrade-1.0 force-response-1.0
# 	</IfModule>

# 	<IfModule mod_rewrite.c>
# 	  RewriteEngine On
# 	  RewriteOptions inherit
# 	</IfModule>

# 	Include /etc/apache2/vhosts.d/kolab-domain-*.include
# 	Include /etc/apache2/vhosts.d/@MYDOMAIN@-domain-*.include
# 	Include /etc/apache2/vhosts.d/@MYDOMAIN@-ssl-domain-*.include

# 	#----------------------------------------------------------------------
# 	# DAV SUPPORT
# 	#
# 	# Allows DAV access to the public /www and the private /dav directories
# 	# If there should be no ssl host, this section needs to be added to the
# 	# upper vhost on port 80
# 	#----------------------------------------------------------------------

# 	<IfModule mod_dav.c>

# 	  Alias /dav /var/www/@MYDOMAIN@/htdocs                                                                                   

# 	  DavMinTimeout 600
# 	  <Location /dav>
# 	  Dav On
# 	  Options None

#         <IfModule mod_authz_host.c>
#           Allow from all
#           <IfModule authnz_ldap_module>
#             AuthType Basic
#             AuthName "Kolab Domain DAV"
#             AuthBasicProvider ldap-mail ldap-uid
#             Require ldap-group "cn=@MYDOMAIN@,cn=domains,cn=internal"
#           </IfModule>
#         </IfModule>
	  
# 	</Location>
#       </IfModule>

#     </VirtualHost>                                  
#   </IfModule>
# </IfDefine>
# </IfDefine>

# #----------------------------------------------------------------------
# # REDIRECTION VHOSTS
# #
# # Vhosts that can be used for conveniently redirection to subservices
# #----------------------------------------------------------------------

# ### Relocates to webadmin frontend

# <VirtualHost *:80>
#   DocumentRoot "/var/www/kolab/htdocs"
#   ServerName admin.@MYDOMAIN@
#   ErrorLog /var/log/apache2/@MYDOMAIN@_error_log
#   CustomLog /var/log/apache2/@MYDOMAIN@_access_log combined
#   @@@if apache-http@@@
#   Redirect  /  http://@MYDOMAIN@/admin
#   @@@else@@@
#   Redirect  /  https://@MYDOMAIN@/admin
#   @@@endif@@@
# </VirtualHost>

# ### Relocates to server portal

# <VirtualHost *:80>
#   DocumentRoot "/var/www/kolab/htdocs"
#   ServerName server.@MYDOMAIN@
#   ErrorLog /var/log/apache2/@MYDOMAIN@_error_log
#   CustomLog /var/log/apache2/@MYDOMAIN@_access_log combined
#   @@@if apache-http@@@
#   Redirect  /  http://@MYDOMAIN@/server
#   @@@else@@@
#   Redirect  /  https://@MYDOMAIN@/server
#   @@@endif@@@
# </VirtualHost>

# ### Relocates to ticket submission and ticket management 
# ### frontends

# <VirtualHost *:80>
#   DocumentRoot "/var/www/kolab/htdocs"
#   ServerName support.@MYDOMAIN@
#   ErrorLog /var/log/apache2/@MYDOMAIN@_error_log
#   CustomLog /var/log/apache2/@MYDOMAIN@_access_log combined
#   @@@if apache-http@@@
#   Redirect  /  http://@MYDOMAIN@/otrs/customer.pl
#   @@@else@@@
#   Redirect  /  https://@MYDOMAIN@/otrs/customer.pl
#   @@@endif@@@
# </VirtualHost>

# <VirtualHost *:80>
#   DocumentRoot "/var/www/kolab/htdocs"
#   ServerName tickets.@MYDOMAIN@
#   ErrorLog /var/log/apache2/@MYDOMAIN@_error_log
#   CustomLog /var/log/apache2/@MYDOMAIN@_access_log combined
#   @@@if apache-http@@@
#   Redirect  /  http://@MYDOMAIN@/otrs/index.pl
#   @@@else@@@
#   Redirect  /  https://@MYDOMAIN@/otrs/index.pl
#   @@@endif@@@
# </VirtualHost>

# ### Relocates to the webmail frontend

# <VirtualHost *:80>
#   DocumentRoot "/var/www/kolab/htdocs"
#   ServerName mail.@MYDOMAIN@
#   ErrorLog /var/log/apache2/@MYDOMAIN@_error_log
#   CustomLog /var/log/apache2/@MYDOMAIN@_access_log combined
#   @@@if apache-http@@@
#   Redirect  /  http://@MYDOMAIN@/horde
#   @@@else@@@
#   Redirect  /  https://@MYDOMAIN@/horde
#   @@@endif@@@
# </VirtualHost>

# Include /etc/apache2/vhosts.d/@MYDOMAIN@-vhosts-*.include
