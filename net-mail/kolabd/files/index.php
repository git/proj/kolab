<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
		"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
   <link 
       rel="icon" 
       type="image/png"
       href="favicon.ico" 
       />

    <title>
      Powered by Kolab on Gentoo
    </title>
  </head>

  <body>

    <div>

      <a style="text-decoration:none;" href="http://www.kolab.org">
        <img style="border: 0px;" src="kolab_logo.small.png" alt="Kolab" />
      </a>

    </div>

    <p style="height:30px;"/>

    <div style='color:#ffffff; font-family: "bitstream vera sans", "verdana", "arial", "helvetica", sans-serif;'>

      <ul>

	<!-- Ugly little PP thingy to get the job done -->
        <?php
           $files = scandir(getcwd() . "/services");
           $services = array();
           foreach ($files as $file) {
             if (preg_match("/.*\.service$/", $file)) {
	       $services[$file] = file(getcwd() . "/services/" . $file);
             }
           }  
           foreach ($services as $service) {
             echo "<li><a href=\"";                                                                                          
             echo $service[0];                                                                                               
             echo "\">";
             echo $service[1];
             echo "</a></li>\n";
           }
	?>

      </ul>

    </div>

    <p style="height:30px;"/>

    <div>

      <a style="text-decoration:none;" href="http://www.gentoo.org">
        <img style="border: 0px;" src="powered-by-gentoo.jpg" alt="Powered by Gentoo" />
      </a>

    </div>

  </body>

</html>

