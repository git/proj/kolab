# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

MY_P=kolabd-2.1-gentoo-20070510

DESCRIPTION="The kolab server"
HOMEPAGE="http://www.kolab.org"
SRC_URI="http://files.pardus.de/${MY_P}.tar.bz2"

SLOT="0"

LICENSE="GPL-2"
KEYWORDS="x86 amd64"
IUSE="horde spam webdav legacy"

DEPEND=""

RDEPEND="
>=net-libs/c-client-2004g-r1
>=dev-lang/php-5.2.6

>net-mail/cyrus-imapd-2.3.12
>net-mail/cyrus-imap-admin-2.3.12

>=app-antivirus/clamav-0.90
>=mail-mta/postfix-2.4.0
>=www-servers/apache-2.2.0
>=net-nds/openldap-2.3.35-r1

dev-perl/URI
dev-perl/perl-ldap
dev-perl/Net-Netmask
dev-perl/TermReadKey

mail-filter/spamassassin
mail-filter/amavisd-new
dev-libs/cyrus-sasl

legacy? (
net-ftp/proftpd
)

spam? (
mail-filter/dcc
mail-filter/razor
dev-python/pyzor
)

>=dev-perl/perl-kolab-2.1

net-mail/kolab-resource-handlers
net-mail/kolab-webadmin
"

S=${WORKDIR}/${MY_P}

detect_skiplist_db() {

	IMAPD_DB_FORMAT="none"

	if [ -f /var/imap/mailboxes.db ]; then
		if [ "`head -c 17 /var/imap/mailboxes.db | tail -c 13`" == "skiplist file" ]; then
			IMAPD_DB_FORMAT="skiplist"
		else
			IMAPD_DB_FORMAT="other"
		fi
	fi

	if [ "${IMAPD_DB_FORMAT}" == "skiplist" ]; then

		ewarn "Your cyrus-imapd databases are in the older \"skiplist\" format."
		ewarn "kolabd-20070216 changes to \"berkeley\" format and your databases"
		ewarn "must be converted! This ebuild will fail until you manually"
		ewarn "converted the files."
		ewarn
		ewarn "These are the necessary commands for the conversion:"
		ewarn
		ewarn "# Ensure that cyrus-imap is not running"
		ewarn "/etc/init.d/cyrus stop"
		ewarn "# Move the old databases away"
		ewarn "mv /var/imap/mailboxes.db /var/imap/mailboxes.db-skiplist"
		ewarn "mv /var/imap/annotations.db /var/imap/annotations.db-skiplist"
		ewarn "# Convert the databases"
		ewarn "/usr/lib/cyrus/cvt_cyrusdb /var/imap/mailboxes.db-skiplist skiplist /var/imap/mailboxes.db berkeley"
		ewarn "/usr/lib/cyrus/cvt_cyrusdb /var/imap/annotations.db-skiplist skiplist /var/imap/annotations.db berkeley"
		ewarn "chown -R cyrus:mail /var/imap/annotations.db /var/imap/mailboxes.db /var/imap/db"
		ewarn "# Emerge the new kolabd ebuild again"
		ewarn "emerge kolabd"
		ewarn "# Update the configuration"
		ewarn "kolabconf"
		ewarn "# Restart the cyrus-imapd server"
		ewarn "/etc/init.d/cyrus start"

		die

	elif [ "${IMAPD_DB_FORMAT}" == "other" ]; then

		einfo "Your cyrus-imapd databases do not seem to have the \"skiplist\""
		einfo "format. This ebuild will now assume they are in berkeley format"
		einfo "which is necessary for kolabd-20070216."

	fi
}

detect_apache_modules() {

	APACHE_MODULES="mod_authn_alias"

	APACHE_BUILTINS="/etc/apache2/apache2-builtin-mods"

	if [ -f "${APACHE_BUILTINS}" ]; then
		for MOD in ${APACHE_MODULES}
		do
			if [ "`grep $MOD ${APACHE_BUILTINS}`" == "" -o "`grep $MOD ${APACHE_BUILTINS} | grep disabled`" != "" ]; then

				ewarn "The module $MOD in your apache configuration file"
				ewarn "${APACHE_BUILTINS} has been set to \"disabled\"."
				ewarn "Please set it to \"shared\" and re-emerge apache."

				die
			else
				einfo "Apache module ${MOD} seems to be activated."
			fi
		done
	else
		einfo "There exists no /etc/apache2/apache2-builtin-mods. The"
		einfo "ebuild is unable to determine the modules build for your"
		einfo "Aapche. Assuming that everyting is okay."
	fi
}

detect_bad_clamav_log() {

	owner=`stat --printf="%U" /var/log/clamav/clamd.log`
	if [ "$owner" != "amavis" ]; then
		ewarn "/var/log/clamav/clamd.log is not owned by amavis!"
		ewarn "Restarting clamav may fail."
		ewarn "Please run"
		ewarn "chown amavis\\: /var/log/clamav/clamd.log"
	fi
}

add_user_to_group() {

	#############################################################
	# HACK:
	# I did not see any other ebuild that adds existing users
	# to new groups. So I'm not certain it is ok to handle it
	# like this since I need to disable the sandbox to access
	# the /etc/group file.
	# The given users need access to the ssl certificates which
	# is possible if they belong to kolab-r

	local oldsandbox=${SANDBOX_ON}
	export SANDBOX_ON="0"
	gpasswd -a $1 $2
	export SANDBOX_ON=${oldsandbox}
}

pkg_setup() {

	if ! built_with_use net-nds/openldap berkdb crypt overlays perl ssl ; then
	eerror "You MUST re-emerge openldap with berkdb, crypt,"
	eerror "overlays, perl and ssl enabled. Please add these use flags"
	eerror "to your USE variable and re-emerge openldap!"
	die "Current openldap installation does not support Kolabd!"
	fi

	if built_with_use net-nds/openldap minimal ; then
	eerror "You MUST re-emerge openldap without the"
	eerror "minimal use flag enabled. Please remove this use flags"
	eerror "from your USE variable and re-emerge openldap!"
	die "Current openldap installation does not support Kolabd!"
	fi

	if ! built_with_use dev-lang/php kolab ; then
	eerror "You MUST re-emerge php with the"
	eerror "kolab use flag enabled. Please add this use flags"
	eerror "to your USE variable and re-emerge php!"
	die "Current php installation does not support Kolabd!"
	fi

	if ! built_with_use dev-libs/cyrus-sasl ldap ssl; then
	eerror "You MUST re-emerge cyrus-sasl with ldap and ssl support."
	eerror "Please add the these use flags"
	eerror "to your USE variable and re-emerge cyrus-sasl!"
	die "Current cyrus-sasl installation does not support Kolabd!"
	fi

	if ! built_with_use mail-mta/postfix sasl ssl ldap ; then
	eerror "You MUST re-emerge postfix with ldap, sasl,"
	eerror "and ssl enabled. Please add these use flags"
	eerror "to your USE variable and re-emerge postfix!"
	die "Current postfix installation does not support Kolabd!"
	fi

	if ! built_with_use net-mail/cyrus-imapd kolab ssl ; then
	eerror "You MUST re-emerge cyrus-imapd with kolab"
	eerror "and ssl enabled. Please add both use flags"
	eerror "to your USE variable and re-emerge cyrus-imapd!"
	die "Current cyrus-imapd installation does not support Kolabd!"
	fi

	if ! built_with_use net-libs/c-client kolab; then
	eerror "You MUST re-emerge c-client with kolab"
	eerror "enabled. Please add the use flag"
	eerror "to your USE variable and re-emerge c-client!"
	die "Current cyrus-imapd installation does not support Kolabd!"
	fi

	if ! built_with_use www-servers/apache ssl ; then
	eerror "You MUST re-emerge apache with ssl support."
	eerror "Please add the ssl use flag"
	eerror "to your USE variable and re-emerge apache!"
	die "Current apache installation does not support Kolabd!"
	fi

	if use legacy ; then
		if ! built_with_use net-ftp/proftpd ssl ldap ; then
			eerror "You MUST re-emerge proftpd with ldap"
			eerror "and ssl enabled. Please add both use flags"
			eerror "to your USE variable and re-emerge proftpd!"
			die "Current proftpd installation does not support Kolabd!"
		fi
	fi

	# Perform some important sanity checks.
	detect_apache_modules
	detect_skiplist_db
	detect_bad_clamav_log

	# This needs to be in pkg_setup so that it will also be done
	# when using binary packages.

	add_user_to_group ldap kolab-r
	add_user_to_group cyrus kolab-r
	add_user_to_group postfix kolab-r
	add_user_to_group apache kolab-r
}

src_unpack() {

	unpack ${A} && cd "${S}"

	for PATCH in ${FILESDIR}/*-${PV}.patch
	do
		for USEFLAG in webdav horde spam
		do
			if [ -z "${PATCH/*-use-${USEFLAG}-${PV}.patch/}" ]; then
				use ${USEFLAG} && epatch ${PATCH} && PATCHED="${PATCHED} ${PATCH/-use-${USEFLAG}/}"
			fi
		done
	done

	for PATCH in ${FILESDIR}/*-${PV}.patch
	do
		patch=1
		for NP in ${PATCHED}
		do
			if [ "${NP}" == "${PATCH}" ]; then
				patch=0
			fi
		done
		if [ -n "${PATCH/*-use-*-${PV}.patch/}" ]; then
			if [ ${patch} -gt 0 ]; then
				epatch ${PATCH}
			fi
		fi
	done

}

src_compile() {

	local myconf
	myconf="${myconf} --with-dist=gentoo"
	myconf="${myconf} --libexecdir=/usr/lib"
	myconf="${myconf} --localstatedir=/var"

	econf ${myconf} || die
	emake || die
}

src_install() {
	make DESTDIR="${D}" install || die "Install failed"

	#############################################################
	# Remove httpd templates
	rm ${D}/etc/kolab/templates/httpd.*

	#############################################################
	# Add additional templates

	cd "${FILESDIR}"

	for TEMPLATE in *-${PV}.template
	do
		for USEFLAG in webdav horde spam
		do
			if [ -z "${TEMPLATE/*.use-${USEFLAG}-${PV}.template/}" ]; then
				if use ${USEFLAG}; then
					cp ${TEMPLATE} ${D}/etc/kolab/templates/${TEMPLATE/.use-${USEFLAG}-${PV}/} \
					&& TEMPLATED="${TEMPLATED} ${TEMPLATE/use-${USEFLAG}-/}"
				fi
			fi
		done
	done

	for TEMPLATE in *.${PV}.template
	do
		template=1
		for NP in ${TEMPLATED}
		do
			if [ "${NP}" == "${TEMPLATE}" ]; then
				template=0
			fi
		done
		if [ -n "${TEMPLATE/*.use-*-${PV}.template/}" ]; then
			if [ ${template} -gt 0 ]; then
				cp ${TEMPLATE} ${D}/etc/kolab/templates/${TEMPLATE/.${PV}/}
			fi
		fi
	done

	for POSTFIX in *.${PV}.postfix
	do
		mkdir -p ${D}/etc/postfix
		cp ${POSTFIX} ${D}/etc/postfix/${POSTFIX/.${PV}.postfix/}
	done

	cd "${S}"

	# Create original dist templates
	for TMPL in	${D}/etc/kolab/templates/*
	do
		cp $TMPL $TMPL.dist
	done

	#############################################################
	# Keep necessary directories

	VARDIRS=$(find ${D}/var -type d)
	for DIR in ${VARDIRS}
	do
		keepdir ${DIR/${D}/}
	done

	#############################################################
	# Provide basic web root

	mkdir -p ${D}/var/www/kolab/htdocs/server/services
	insinto /var/www/kolab/htdocs/server/
	insopts --mode=0644 --owner=root --group=root
	doins ${FILESDIR}/favicon.ico
	doins ${FILESDIR}/kolab_logo.small.png
	doins ${FILESDIR}/powered-by-gentoo.jpg
	doins ${FILESDIR}/index.php

	# now add the services to the service directory
	insinto /var/www/kolab/htdocs/server/services
	insopts --mode=0644 --owner=root --group=root
	doins ${FILESDIR}/admin.service

	if use horde ; then
		doins ${FILESDIR}/horde.service
	fi

	#############################################################
	# Fix permissions

	fowners apache:apache /var/cache/httpd_sessions

	fowners kolab:kolab /etc/kolab/kolab.globals

	insinto /etc/init.d
	insopts --mode=0755 --owner=root --group=root
	if use legacy ; then
		cp ${FILESDIR}/kolabd.legacy ${S}/kolabd
		doins ${S}/kolabd
	else
		doins ${FILESDIR}/kolabd
		rm ${D}/etc/kolab/templates/proftpd.conf.template*
	fi

	# FIXME
	# This is kolab-resource-handlers stuff. But I don't want to fix the
	# dependancies right now.
	keepdir /var/cache/freebusy/
	fowners apache:apache /var/cache/freebusy/

	keepdir /var/log/kolab/freebusy
	fowners apache:apache /var/log/kolab/freebusy

	keepdir /var/log/kolab/resmgr
	fowners kolab-n:kolab-n /var/log/kolab/resmgr

	keepdir /var/amavis/clamav
	fowners amavis:amavis /var/amavis/clamav
}


pkg_postinst() {

	echo
	ewarn "This new version changes the format of some central cyrus-imapd databases."
	ewarn "The ebuild tries to automatically detect any problems but please be aware"
	ewarn "that this may fail and you need to debug your cyrus-admin configuration!"
	echo
	ewarn "You need to execute "
	ewarn ""
	ewarn "/usr/share/${PN}/scripts/kolab_bootstrap -b"
	ewarn ""
	ewarn "to complete the kolab installation"
	echo

	if use horde; then
		echo
		ewarn "*** USE FLAG HORDE ***"
		ewarn
		ewarn "This use flag will install horde for use with kolab and"
		ewarn "in addition pathes the server for the horde installation."
		ewarn
		ewarn "Please note that the horde packages are NOT AT ALL in a "
		ewarn "stable state. Do not expect horde to work on your machine."
		echo
	fi

	if use spam; then
		echo
		ewarn "*** USE FLAG SPAM ***"
		ewarn
		ewarn "If you wish to register with razor so that you can also report spam,"
		ewarn "you can run"
		ewarn "mkdir -p /etc/mail/razor && chown amavis:amavis /etc/mail/razor"
		ewarn "su -s /bin/bash - amavis razor-admin -home=/etc/mail/razor -register my@mail.com"
		ewarn "su -s /bin/bash - amavis razor-admin -home=/etc/mail/razor -create"
		ewarn "Edit /etc/mail/razor/razor-agent.conf afterwards and link to the newly created identity"
		echo
	fi

}
