# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils php-lib

MY_P=kolab-resource-handlers-2.1-gentoo-20070510

DESCRIPTION="The kolab resource manager"
HOMEPAGE="http://www.kolab.org"
SRC_URI="http://files.pardus.de/${MY_P}.tar.bz2"

SLOT="0"

LICENSE="GPL-2"
KEYWORDS="x86 amd64"
IUSE=""

DEPEND=""

RDEPEND="${DEPEND}
>=net-libs/c-client-2004g-r1
>=dev-lang/php-5.2.6

>=dev-php/PEAR-PEAR-1.4.6-r1
>=dev-php/PEAR-Net_SMTP-1.2.7
>=dev-php/PEAR-Net_LMTP-1.0.1-r1
>=dev-php/PEAR-Net_IMAP-1.0.3-r2
>=dev-php/PEAR-Mail_Mime-1.3.1-r1
dev-php/horde-framework-kolab
"

S=${WORKDIR}/${MY_P}

src_unpack() {

	unpack ${A} && cd "${S}"

	for PATCH in ${FILESDIR}/*-${PV}.patch
	do
		epatch ${PATCH}
	done

}

src_compile() {

	local myconf
	myconf="${myconf} --with-dist=gentoo"
	myconf="${myconf} --libexecdir=/usr/lib"
	myconf="${myconf} --localstatedir=/var"

	econf ${myconf} || die
	emake || die

}

src_install() {
	make DESTDIR="${D}" install || die "Install failed"
}
