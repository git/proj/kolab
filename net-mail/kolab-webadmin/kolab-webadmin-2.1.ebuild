# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# Marked STABLE (comment necessary for update script)

inherit eutils depend.php

MY_P=${PN}-2.1-gentoo-20070528

DESCRIPTION="The kolab webadmin frontend"
HOMEPAGE="http://www.kolab.org"
SRC_URI="http://files.pardus.de/${MY_P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="x86 amd64"
IUSE=""

DEPEND=""

RDEPEND="
>=dev-php/smarty-2.6.12
>=dev-php/PEAR-PEAR-1.4.6-r1
>=dev-php/PEAR-Auth_SASL-1.0.1-r1
>=dev-php/PEAR-Net_Socket-1.0.6-r1
"

S=${WORKDIR}/${MY_P}

need_php

pkg_setup() {

	require_php_with_use ldap nls

}

src_unpack() {

	unpack ${A} && cd "${S}"

	for PATCH in ${FILESDIR}/*-${PV}.patch
	do
		epatch ${PATCH}
	done

}

src_compile() {

	local myconf
	myconf="${myconf} --with-dist=gentoo"
	myconf="${myconf} --libexecdir=/usr/lib"
	myconf="${myconf} --localstatedir=/var"

	econf ${myconf} || die
	emake || die
}

src_install() {

	make DESTDIR="${D}" install || die "Install failed"

	keepdir /var/www/kolab/htdocs/admin/templates_c
	fowners apache:apache /var/www/kolab/htdocs/admin/templates_c
}
