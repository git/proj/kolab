#################################################
# YOU MAY UNMASK THESE - STANDARD DEVELOPMENT
#################################################

#################################################
# DO NOT UNMASK THESE - IT MIGHT BREAK THE SERVER
#################################################

# Gunnar Wrobel <wrobel@pardus.de> (05 May 2008)
# Early preparations for Kolab2/Gentoo-2.2
# Do not mix these packages with Kolab2/Gentoo-2.1!
app-admin/pardalys
>dev-perl/perl-kolab-2.1
>=dev-php/horde-framework-kolab-3.2_rc3
