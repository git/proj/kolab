# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit php-pear-r1 eutils

PEAR_PV="${PV/_/}"
PEAR_PN="${PHP_PEAR_PKG_NAME/Horde_/}-${PEAR_PV}"

HOMEPAGE="http://www.kolab.org"
DESCRIPTION="Classes and methods for filtering groupware relevant mail messages."
SRC_URI="http://pear.horde.org/get/${PEAR_PN}.tgz"

SLOT="0"

LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="dev-php/PEAR-Horde-Channel"

RDEPEND="${RDEPEND}
	>=dev-php/PEAR-Net_SMTP-1.2.7
	>=dev-php/PEAR-Net_LMTP-1.0.1-r1
	>=dev-php/PEAR-Mail_Mime-1.3.1-r1
	>=dev-php/Horde_Framework-0.0.2
	>=dev-php/Horde_MIME-0.0.2
	>=dev-php/Horde_Argv-0.0.2
	>=dev-php/Horde_Util-0.0.2
	>=dev-php/Horde_iCalendar-0.1.0
	dev-php/Horde_Kolab_Format
	dev-php/Horde_Kolab_Server
	dev-php/Horde_Kolab_Storage"

S="${WORKDIR}/${PEAR_PN}"
