# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit php-pear-r1 eutils

PEAR_PV="${PV/_/}"
PEAR_PN="${PHP_PEAR_PKG_NAME/Horde_/}-${PEAR_PV}"

HOMEPAGE="http://www.horde.org"
DESCRIPTION="Horde Authentication API"
SRC_URI="http://pear.horde.org/get/${PEAR_PN}.tgz"

SLOT="0"

LICENSE="LGPL-2"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="dev-php/PEAR-Horde-Channel
	dev-php/Horde_Framework
	dev-php/Horde_Secret
	dev-php/Horde_Util"

RDEPEND="${DEPEND}"

S="${WORKDIR}/${PEAR_PN}"
