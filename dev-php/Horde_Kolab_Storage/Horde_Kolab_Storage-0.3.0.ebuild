# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit php-pear-r1 eutils

PEAR_PV="${PV/_/}"
PEAR_PN="${PHP_PEAR_PKG_NAME/Horde_/}-${PEAR_PV}"

HOMEPAGE="http://www.horde.org"
DESCRIPTION="A package for handling Kolab data stored on an IMAP server."
SRC_URI="http://pear.horde.org/get/${PEAR_PN}.tgz"

SLOT="0"

LICENSE="LGPL-2"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="${DEPEND}
	dev-php/PEAR-Horde-Channel"

RDEPEND="${RDEPEND}
	dev-php/Horde_Auth
	dev-php/Horde_Cache
	dev-php/Horde_Group
	dev-php/Horde_History
	dev-php/Horde_LDAP
	dev-php/Horde_Perms
	dev-php/Horde_SessionObjects
	dev-php/Horde_MIME
	dev-php/Horde_NLS
	dev-php/Horde_Util
	dev-php/Horde_Kolab_Format
	dev-php/Horde_Kolab_Server"

S="${WORKDIR}/${PEAR_PN}"
