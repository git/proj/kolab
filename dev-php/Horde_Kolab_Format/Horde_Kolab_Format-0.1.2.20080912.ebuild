# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit php-pear-r1 eutils

PEAR_PV="${PV/_/}"
PEAR_PN="${PHP_PEAR_PKG_NAME/Horde_/}-${PEAR_PV}"

HOMEPAGE="http://www.horde.org"
DESCRIPTION="A package for reading/writing Kolab data formats."
SRC_URI="http://files.pardus.de/kolab-sources/${PEAR_PN}.tgz"

SLOT="0"

LICENSE="LGPL-2"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="${DEPEND}
	dev-php/PEAR-Horde-Channel"

RDEPEND="${RDEPEND}
	dev-php/Horde_DOM
	dev-php/Horde_NLS
	dev-php/Horde_Util"

S="${WORKDIR}/${PEAR_PN}"
