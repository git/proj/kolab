# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit php-pear-r1 eutils depend.php

PEAR_PV="${PV/_/}"
PEAR_PN="${PHP_PEAR_PKG_NAME/Horde_/}-${PEAR_PV}"

HOMEPAGE="http://www.horde.org"
DESCRIPTION="A package for manipulating the Kolab user database."
SRC_URI="http://pear.horde.org/get/${PEAR_PN}.tgz"

SLOT="0"

LICENSE="LGPL-2"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="${DEPEND}
	dev-php/PEAR-Horde-Channel"

RDEPEND="${RDEPEND}
	dev-php/Horde_LDAP"

S="${WORKDIR}/${PEAR_PN}"

pkg_setup() {
	   require_php_with_use ldap
}

