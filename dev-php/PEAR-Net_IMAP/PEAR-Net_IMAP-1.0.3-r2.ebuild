# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-php/PEAR-Net_IMAP/PEAR-Net_IMAP-1.0.3-r1.ebuild,v 1.14 2006/02/18 20:14:58 agriffis Exp $

inherit php-pear-r1 eutils

DESCRIPTION="Provides an implementation of the IMAP protocol."

LICENSE="PHP"
SLOT="0"
KEYWORDS="x86 ~amd64"
IUSE=""
RDEPEND=">=dev-php/PEAR-Net_Socket-1.0.6-r1"

src_unpack() {

	unpack "${A}"

	cd "${S}"

	epatch ${FILESDIR}/imap-logout.patch

}
