# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-php/PEAR-Auth/PEAR-Auth-1.3.2.ebuild,v 1.1 2006/09/17 13:52:44 chtekk Exp $

inherit php-pear-r1 depend.php

DESCRIPTION="Dummy ebuild for an additional Horde PEAR channel."
LICENSE="PHP"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""
RDEPEND=""

SRC_URI=""

src_unpack() { :;}
src_install() { :;}

pkg_postinst() {

		pear clear-cache

		# Update PEAR/PECL channels as needed, add new ones to the list if needed
		pearchans="pear.horde.org"

		for chan in ${pearchans} ; do
				pear channel-discover ${chan}
				pear channel-update ${chan}
		done
}
