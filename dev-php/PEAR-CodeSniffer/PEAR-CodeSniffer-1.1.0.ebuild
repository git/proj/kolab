# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-php/PEAR-Net_Sieve/PEAR-Net_Sieve-1.1.6.ebuild,v 1.7 2008/05/09 22:20:51 maekke Exp $

PHP_PEAR_PKG_NAME="${PN/PEAR-/PHP_}"

inherit php-pear-r1

DESCRIPTION="PHP_CodeSniffer tokenises PHP and JavaScript code and detects violations of a defined set of coding standards."

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
