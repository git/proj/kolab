# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils php-pear-manylibs-r1

MY_PN=${PN/-kolab/}
MY_PV=${PV/alpha/ALPHA}
MY_P=${MY_PN}-${MY_PV}

DESCRIPTION="Horde Application Framework"
HOMEPAGE="http://www.horde.org/"
SRC_URI="http://files.pardus.de/${MY_P}.tar.bz2"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="x86 amd64"
IUSE=""

S=${WORKDIR}/framework

HORDE_PHP_FEATURES="imap ldap nls session xml"

PEAR_PACKAGES="
Alarm
Auth
Block
Browser
Cache
Cipher
CLI
Compress
Crypt
Data
DataTree
Date
devtools
DOM
Editor
Feed
File_CSV
File_PDF
Form
Group
History
Horde
Http_Client
iCalendar
Image
IMAP
Kolab
LDAP
Maintenance
Memcache
MIME
Mobile
Net_IMSP
Net_SMS
NLS
Notification
Perms
Prefs
Rdo
RPC
Scheduler
Secret
Serialize
SessionHandler
SessionObjects
Share
SpellChecker
SQL
SyncML
Template
Template_Compiling
Text_Diff
Text_Filter
Text_Flowed
Text_reST
Token
Tree
UI
Util
VC
VFS
VFS_ISOWriter
View
Xml_Element
XML_WBXML
Yaml
"

RDEPEND="${RDEPEND}
!dev-php/horde
!dev-php/horde-framework-cvs
dev-php/PEAR-Horde-Channel
"


pkg_setup() {
	if [[ ! -z ${HORDE_PHP_FEATURES} ]] ; then
		local param
		if [[ ${HORDE_PHP_FEATURES:0:2} = "-o" ]] ; then
			param="-o"
			HORDE_PHP_FEATURES=${HORDE_PHP_FEATURES:2}
		fi
		if ! built_with_use ${param} dev-lang/php ${HORDE_PHP_FEATURES} ; then
			if [[ ${parm} == "-o" ]] ; then
				eerror "You MUST re-emerge php with at least one of"
			else
				eerror "You MUST re-emerge php with all of"
			fi
			eerror "the following options in your USE:"
			eerror " ${HORDE_PHP_FEATURES}"
			die "current php install cannot support ${HORDE_PN}"
		fi
	fi
}

src_unpack() {

	unpack ${A} && cd "${S}"

	for PATCH in "${FILESDIR}"/*-${MY_PV}.patch
	do
		epatch "${PATCH}"
	done
}

pkg_postinst() {
	einfo "Horde requires PHP to have:"
	einfo "    ==> 'short_open_tag enabled = On'"
	einfo "    ==> 'magic_quotes_runtime set = Off'"
	einfo "    ==> 'file_uploads enabled = On'"
	einfo "Please edit /etc/php/apache2-php5/php.ini"
}
