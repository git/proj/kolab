# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit php-pear-r1 eutils

PEAR_PV="${PV/_/}"
PEAR_PN="${PHP_PEAR_PKG_NAME}-${PEAR_PV}"

HOMEPAGE="http://www.horde.org"
DESCRIPTION="Horde CLI API"
SRC_URI="http://pear.horde.org/get/${PEAR_PN}.tgz"

SLOT="0"

LICENSE="LGPL-2"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="dev-php/PEAR-Horde-Channel"

RDEPEND="${DEPEND}"

S="${WORKDIR}/${PEAR_PN}"
