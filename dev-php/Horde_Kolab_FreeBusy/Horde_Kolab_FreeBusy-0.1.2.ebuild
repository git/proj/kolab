# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit php-pear-r1 eutils

PEAR_PV="${PV/_/}"
PEAR_PN="${PHP_PEAR_PKG_NAME/Horde_/}-${PEAR_PV}"

HOMEPAGE="http://www.horde.org"
DESCRIPTION="A package for providing free/busy information."
SRC_URI="http://pear.horde.org/get/${PEAR_PN}.tgz"

SLOT="0"

LICENSE="LGPL-2"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="${DEPEND}
	>=dev-php/PEAR-PEAR-1.7.0
	dev-php/PEAR-Horde-Channel"

RDEPEND="${RDEPEND}
	dev-php/Horde_Date
	dev-php/Horde_iCalendar
	dev-php/Horde_Kolab_Format
	dev-php/Horde_Kolab_Server
	dev-php/Horde_Kolab_Storage
	www-servers/apache"

S="${WORKDIR}/${PEAR_PN}"

src_install() {

	php-pear-r1_src_install

	keepdir /var/cache/kolab-freebusy
	fperms 0750 /var/cache/kolab-freebusy
	fowners apache:apache /var/cache/kolab-freebusy
}
