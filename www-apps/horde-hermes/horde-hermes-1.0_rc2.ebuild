# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/www-apps/horde-nag/horde-nag-2.2.ebuild,v 1.1 2008/06/01 21:47:04 vapier Exp $

HORDE_MAJ="-h3"
inherit horde

DESCRIPTION="Hermes is a time-tracking application integrated with the Horde Framework."

HORDE_PATCHSET_REV=1

SRC_URI="${SRC_URI}
        http://files.pardus.de/hermes-kolab-patches-${PV}-r${HORDE_PATCHSET_REV}.tar.bz2"

KEYWORDS="~amd64 ~x86"
IUSE="kolab"

EHORDE_PATCHES="$(use kolab && echo ${WORKDIR}/hermes-kolab.patch)"

DEPEND=""
RDEPEND="|| ( =www-apps/horde-webmail-1.2* )"

