# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="p@rdalys provides the full configuration set for the Kolab Server and is based on puppet."
HOMEPAGE="http://pardalys.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"

SLOT="0"

LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64"
IUSE=""

#FIXME: Consider moving mocha and rspec to DEPEND later as they are only RDEPEND during dev phase
DEPEND=""
RDEPEND="dev-ruby/ruby-ldap
	dev-ruby/mocha
	dev-ruby/rspec
	>=app-admin/puppet-0.24.4-r1"

S=${WORKDIR}/${PN}

src_install() {

	dosbin "${S}"/bin/pardalys
	dobin "${S}"/bin/dev-pardalys

	insinto /usr/share/${PN}
	doins -r "${S}"/etc
	doins -r "${S}"/modules
	doins -r "${S}"/manifests

	dodir /usr/share/${PN}/facter

	# Link in facts
	for FACT in modules/**/plugins/facter/*.rb
	do
	  dosym /usr/share/${PN}/$FACT /usr/share/${PN}/facter
	done

	dodoc INSTALL HACKING README CHANGES
}
