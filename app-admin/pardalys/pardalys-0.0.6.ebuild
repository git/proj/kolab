# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="p@rdalys provides the full configuration set for the Kolab Server and is based on puppet."
HOMEPAGE="http://pardalys.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"

SLOT="0"

LICENSE="GPL-2"
KEYWORDS="~x86 ~amd64"
IUSE="develop"

DEPEND=""
RDEPEND="dev-ruby/ruby-ldap
	>=app-admin/puppet-0.24.4-r1
	develop? ( dev-util/git )"

S=${WORKDIR}/${PN}

src_compile() {
	einfo "No compilation necessary."
}

src_install() {
	emake \
	  DESTDIR="${D}" \
	  prefix="/usr" \
	  datadir="/usr/share/" \
	  docdir="/usr/share/doc/${P}" \
	  sysconfdir="/etc" \
	  install
}
