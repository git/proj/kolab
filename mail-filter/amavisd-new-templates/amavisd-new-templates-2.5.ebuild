# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Templates for amavisd-new"
HOMEPAGE="http://www.kolab.org"
SRC_URI="http://files.pardus.de/${P}.tar.bz2"

SLOT="0"

LICENSE="GPL-2"
KEYWORDS="x86 amd64"
IUSE=""

DEPEND=""

RDEPEND="mail-filter/amavisd-new"

S="${WORKDIR}"

src_install() {
	insinto /var/amavis
	insopts --owner=amavis --group=amavis
	doins -r .
}
