# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-ruby/rake/rake-0.8.3.ebuild,v 1.1 2008/10/06 15:54:21 graaff Exp $

inherit gems

DESCRIPTION="UUIDTools was designed to be a simple library for generating any of the various types of UUIDs."
HOMEPAGE="http://rubyforge.org/projects/uuidtools/"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
