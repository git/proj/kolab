# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-ruby/rake/rake-0.8.3.ebuild,v 1.1 2008/10/06 15:54:21 graaff Exp $

inherit gems

DESCRIPTION="Amazon Web Services Elastic Compute Cloud (EC2) Ruby Gem"
HOMEPAGE="http://github.com/grempe/amazon-ec2/tree/master"

LICENSE="Ruby"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-ruby/uuidtools"
RDEPEND="${DEPEND}"
